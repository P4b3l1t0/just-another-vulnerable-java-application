#!/usr/bin/env python3

from pytm.pytm import TM, Server, Datastore, Dataflow, Boundary, Actor, Lambda, Data, Classification

tm = TM("Vulnado TM")
tm.description = "Vulnaado"
tm.isOrdered = True

# Definición de límites
Internet = Boundary("Internet")
Public_Subnet = Boundary("Public Subnet")
Private_Subnet = Boundary("Private Subnet")

# Actores
attacker = Actor("Attacker")
attacker.inBoundary = Internet

api = Server("API")
api.OS = "UnknownOS"
api.inBoundary = Public_Subnet

web = Server("Web Server")
web.OS = "UnknownOS"
web.inBoundary = Public_Subnet

db = Datastore("Postgres")
db.OS = "UnknownOS"
db.isHardened = False
db.inBoundary = Private_Subnet
db.isSql = True

internal_website = Server("Internal Website")
internal_website.OS = "UnknownOS"
internal_website.inBoundary = Private_Subnet

# Flujo de datos
attacker_to_api = Dataflow(attacker, api, "Attacker accesses API")
attacker_to_api.protocol = "HTTP"
attacker_to_api.dstPort = 8080

api_to_web = Dataflow(api, web, "API connects to Web Server")
api_to_web.protocol = "HTTP"
api_to_web.dstPort = 80

web_to_db = Dataflow(web, db, "Web Server interacts with DB")
web_to_db.protocol = "SQL"

web_to_internal_website = Dataflow(web, internal_website, "Web Server interacts with Internal Website")
web_to_internal_website.protocol = "HTTP"
web_to_internal_website.dstPort = 80

tm.process()